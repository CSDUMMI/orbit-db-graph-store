let nodes = [...Array(100)].map((_,i) => i.toString())

/**
 * Given a node give the node to create an edge.
 */
function edgeNode(i) {
    return nodes[parseInt(i) + 5] == undefined
        ? nodes[parseInt(i) - 5]
        : nodes[parseInt(i) + 5]
}

module.exports = {
    nodes: nodes,
    edgeNode: edgeNode,
    edgesNum: 50,
}
