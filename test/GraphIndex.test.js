const GraphIndex = require("../src/GraphIndex")
const assert = require("assert")
const { edges, nodes, edgeNode } = require("./nodes")

let graph

describe("Test Graph", () => {
    before(() => {
        graph = new GraphIndex()
    })
    
    it("Add nodes", () => {
        for(let i = 0; i < nodes.length; i++) {
            assert.equal(graph.addNode(i), true)
        }
    })

    it("Add edges", () => {
        for(let i = 0; i < nodes.length; i++) {
            assert.equal(graph.addEdge(nodes[i], edgeNode(i, nodes) , weight = 1/i), true)
        }
    })

    it("Each node should have 1 neighbor and that neigbor be to the correct node.", () => {
        for(let i = 0; i < nodes.length; i++) {
            let neighbors = graph.neighbors(nodes[i])
            assert.equal(neighbors.length, 1, "length")
            assert.equal(neighbors[0].fromName, nodes[i], "fromName")
            assert.equal(neighbors[0].toName, edgeNode(i, nodes), "toName")
            assert.equal(neighbors[0].weight, 1/i, "weight")
        }
    })

    it("No edge should have a weight of undefined", () => {
        graph.allEdges().forEach(edge => {
            assert.notEqual(edge.weight, undefined)
        })
    })

    it("Each node should be contained in the graph", () => {
        for(let i = 0; i < nodes.length; i++) {
            assert.equal(graph.contains(nodes[i]), true)
        }
    })

    it("All nodes should be an array [0..nodes-1]", () => {
        assert.deepEqual(graph.allNodes(), nodes)
    })

    it("All edges should be between i and i + 5 if i < nodes - 5 else i - 5", () => {
        let edges = graph.allEdges()

        edges.forEach(edge => {
            let fromName = parseInt(edge.fromName)
            let toName = parseInt(edge.toName)

            assert.equal(toName.toString(), edgeNode(fromName, nodes))
        })
    })

    it("Removing edges", () => {
        for(let i = 0; i < nodes.length; i++) {
            assert.equal(graph.removeEdge(nodes[i], edgeNode(i, nodes)), true)
            assert.equal(graph.neighbors(nodes[i]).length, 0)
        }
    })

    it("All edges should be []", () => {
        assert.deepEqual(graph.allEdges(), [])
    })

    it("Remove nodes", () => {
        for(let i = 0; i < nodes.length; i++) {
            assert.equal(graph.removeNode(nodes[i]), true)
            assert.equal(graph.contains(nodes[i]), false)
        }
    })

    it("All nodes should be []", () => {
        assert.deepEqual(graph.allNodes(), [])
    })
       
})
