"use strict"

const assert = require("assert")
const GraphStore = require("../src/GraphStore")

const IPFS = require("ipfs")
const OrbitDB = require("orbit-db")
const { nodes, edgesNum } = require("./nodes")
OrbitDB.addDatabaseType(GraphStore.type, GraphStore)

let dbname = "graph-testing"
let ipfs, orbitdb, db

describe("Test GraphStore", function() {
    this.timeout(0)

    before(async () => {
        ipfs = await IPFS.create({ silent: true })
        orbitdb = await OrbitDB.createInstance(ipfs, { directory: "./orbitdb" + Math.random() })
        db = await orbitdb.create(dbname, GraphStore.type)
    })
    
    it("Adding nodes", async () => {
        for(let i = 0; i < nodes.length; i++) {
            await db.addNode(nodes[i])
            assert.equal(db.contains(nodes[i]), true)
        }
    })

    it("Adding edges", async () => {
        for(let i = 0; i < nodes; i++) {
            await db.addEdge(nodes[i], edgeNode(i), weight = 1/i)
            assert.equal(db.neighbors(i).length, 1)
        }
    })

    it("Remove edges", async () => {
        for(let i = 0; i < nodes; i++) {
            await db.removeEdge(nodes[i], edgeNode(i))
            let neighbors = db.neighbors(nodes[i])
            assert.equal(neighbors.length, 0, `Length of neighbors: #${i} ${neighbors[0]} ${neighbors.length}`)
        }
    })

    it("Remove nodes", async () => {
        for(let i = 0; i < nodes.length; i++) {
            await db.removeNode(nodes[i])
        }

        for(let i = 0; i < nodes.length; i++) {
            assert.equal(db.contains(nodes[i]), false)
        }
    })

    it("No edges in db", () => {
        assert.deepEqual(db.allEdges, [])
    })

    it("No nodes in db", () => {
        assert.deepEqual(db.allNodes, [])
    })
})
