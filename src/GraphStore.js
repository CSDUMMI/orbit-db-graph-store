"use strict"

const Store = require("orbit-db-store")
const GraphIndex = require("./GraphIndex")

class GraphStore extends Store {
    constructor(ipfs, id, dbname, options) {
        let opts = Object.assign({}, { Index: GraphIndex })
        Object.assign(opts, options)
        super(ipfs, id, dbname, opts)
        this._type = GraphStore.type
    }

    /**
     * Add a node to the graph.
     * @param name of the node
     */
    addNode(name, options = {}) {
        return this._addOperation({
            op: "ADDNODE",
            name: name
        }, options)
    }

    /**
     * Remove a node from the graph
     * @param name of the node
     */
    removeNode(name, edges = false, options = {}) {
        return this._addOperation({
            op: "REMOVENODE",
            name: name,
            edges: edges,
        }, options)
    }

    /**
     * Add an edge
     * @param fromName name of the node from which the edge should be.
     * @param toName name of the node to which the edge should be.
     * @param weight of the edge
     */
    addEdge(fromName, toName, weight = null, options = {}) {
        return this._addOperation({
            op: "ADDEDGE",
            fromName: fromName,
            toName: toName,
            weight: weight,
         }, options)
    }

    /**
     * @param fromName of the edge to remove
     * @param toName of the edge to remove
     */
    removeEdge(fromName, toName, options = {}) {
        return this._addOperation({
            op: "REMOVEEDGE",
            fromName: fromName,
            toName: toName,
        }, options)
    }

    /**
     * Alias of addNode
     */
    set(name, options = {}) {
        return this.addNode(name, options)
    }

    
    static get type() {
        return "graphstore"
    }

    /**
     * @param fromName node to get neighbors for.
     * @returns {Array<Edge>} Array of the edges from this node to others.
     */
    neighbors(fromName) {
        return this._index.neighbors(fromName)
    }

    /**
     * @returns {boolean} true if a node with that name is contained in the graph.
     */
    contains(name) {
        return this._index.contains(name)
    }

    /**
     * @returns {Array<name>} Array of the names of all the nodes in the graph at this point in time.
     */
    get allNodes() {
        return this._index.allNodes()
    }

    /**
     * @returns {Array<Edge>} all the edges currently in the graph.
     */
    get allEdges() {
        return this._index.allEdges()
    }
}

module.exports = GraphStore
