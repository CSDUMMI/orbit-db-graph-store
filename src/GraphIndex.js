"use strict"


/**
 * Edges, if returned are returned as instances of this class.
 */
class Edge {
    constructor(fromName, toName, weight) {
        this.fromName = fromName
        this.toName = toName
        this.weight = weight
    }

    toJSON() {
        return {
            fromName: this.fromName,
            toName: this.toName,
            weight: this.weight,
        }
    }
    
    toString() {
        return JSON.stringify(this.toJSON())
    }
}

/**
 * Implementation of a directed Graph.
 * Both nodes and edges can be labelled with arbitrary data.
 *
 */
class GraphIndex {
    constructor() {
        this._index = {}
    }

    /**
     * @param name of the node.
     * @return {boolean} true if the node could be created.
     */
    addNode(name) {
        if(this.contains(name)) {
            return false
        }
        
        this._index[name] = {}
        return true
    }

    /**
     * @param name of the node
     * @param edges {boolean} defaults to false. If true edges to this node will be removed too.
     * This increases the runtime from O(1) to O(N + 1)
     * @return {boolean} true if node succesfully removed.
     */
    removeNode(name, edges = false) {
        if(this.contains(name)) {
            if(edges) {
                Object.keys(this._index).forEach(n => {
                    if(this._index[n][name] !== undefined) {
                        this._index[n][name] = undefined
                    }
                })
            }
            this._index[name] = undefined
            return true
        }
        return false
    }

    /**
     * Add a directed edge between fromName and toName.
     */
    addEdge(fromName, toName, weight = null) {
        this.addNode(fromName)
        this.addNode(toName)

        if(this._index[fromName][toName] == undefined) {
            this._index[fromName][toName] = weight
            return true
        }
        return false
    }

    /**
     * Remove edge between fromName and toName
     */
    removeEdge(fromName, toName) {
        if(this.contains(fromName) &&
           this.neighbors(fromName).map(e => e.toName).includes(toName)) {
            this._index[fromName][toName] = undefined
            return true
        }
        return false
    }

    neighbors(fromName) {
        if(this.contains(fromName)) {
            return Object
                .keys(this._index[fromName])
                .filter(toName => this._index[fromName][toName] !== undefined)
                .map(toName => new Edge(fromName, toName, this._index[fromName][toName]))
        }
    }

    contains(name) {
        return this._index[name] !== undefined
    }

    

    allNodes() {
        return Object.keys(this._index).filter(name => this.contains(name))
    }

    allEdges() {
        let edges = []
        Object.keys(this._index)
            .filter(fromName => this.contains(fromName))
            .forEach(fromName => {
                Object.keys(this._index[fromName]).forEach(toName => {
                    let weight = this._index[fromName][toName]
                    if(weight !== undefined) {
                        edges.push(new Edge(fromName, toName, weight))
                    }
                })
            })
        return edges
    }

    updateIndex(oplog) {
        this._index = {}
        oplog.values
            .slice()
            .reduce((handled, entry) => {
                if(!handled.includes(entry.hash)) {
                    handled.push(entry.hash)

                    switch(entry.payload.op) {
                    case "ADDNODE":
                        this.addNode(entry.payload.name)
                        break

                    case "REMOVENODE":
                        this.removeNode(entry.payload.name, entry.payload.edges)
                        break
                        
                    case "ADDEDGE":
                        this.addEdge(entry.payload.fromName, entry.payload.toName, weight = entry.payload.weight)
                        break

                    case "REMOVEEDGE":
                        this.removeEdge(entry.payload.fromName, entry.payload.toName)
                        break
                    default:
                        console.log(`Unknown oplog entry: ${entry.payload}`)
                        break        
                    }
                    
                }
                
                return handled
            }, [])
    }
}

module.exports = GraphIndex
