# orbit-db-graph-store
> Graph database for orbit-db

A directed graph database of orbit-db.

# Table of Contents

- [Install](#install)
- [Usage](#usage)
- [API](#API)
- [Contributing](#contributing)
- [License](#license)

# Install
```
npm i orbit-db-graph-store orbit-db ipfs
```

# Usage
First, create an instance of OrbitDB:
```js
const IPFS = require("ipfs")
const OrbitDB = require("orbit-db")
const GraphStore = require("orbit-db-graph-store")

OrbitDB.addDatabaseType(GraphStore.type, GraphStore)

async function main() {
    const ipfs = await IPFS.create()
    const orbitdb = await OrbitDB.createInstance(ipfs)
}
main()
```

Get a graph database and add nodes and connect them with an edge.:
```js
const graph = await orbitdb.create("example-db", GraphStore.type)

await graph.addNode("A")
await graph.addNode("B")
await graph.addEdge("A","B", weight = 1)
```
Later you can get the neighbors of a node and check if a node is contained in the graph:
```js

graph.neighbors("A")
// [ Edge { fromName: 'A', toName: 'B', weight: 1 } ]
```

# API
TODO 

# Contributing
Feel free to [contribute](https://gitlab.com/CSDUMMI/orbit-db-graph-store) and [open an issue](https://gitlab.com/CSDUMMI/orbit-db-graph-store/-/issues/new)

# License
[GPL 3.0 or later](https://gitlab.com/CSDUMMI/orbit-db-graph-store/-/blob/main/LICENSE) (C) CSDUMMI 2021.
